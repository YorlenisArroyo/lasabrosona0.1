import time

if __name__ == "__main__":
    class cliente:
        def __int__(self, Nombre, Cedula):
            self.Nombre = Nombre
            self.Cedula = Cedula


    print("FONDA LA SABROSONA")
    print("------------------\n")

    ventas_real_time = 0
    Lista_comida = ["mondongo", "Lengua", "Pata"]
    Lista_Precio = [1.50, 0.75, 2.0]

    veces = int(input("Cuentas ventas deseas introducir: "))
    Ventas = open("/home/Sebastianrza/Desktop/Practica/La_Sabrosona /Ventas.txt", "w")
    Ventas.close()
    No_Completado = open("/home/Sebastianrza/Desktop/Practica/La_Sabrosona /Incompletas.txt", "w")
    No_Completado.close()

    for i in range(veces):

        while True:
            Nombre = cliente()
            Nombre.Nombre = input("Introduzca Nombre: ")

            print("1." + str(Lista_comida[0]), str(Lista_Precio[0]) + "$")
            print("2." + str(Lista_comida[1]), str(Lista_Precio[1]) + "$")
            print("3." + str(Lista_comida[2]), str(Lista_Precio[2]) + "$")
            try:
                opcion = int(input("Opcion: "))
            except:
                opcion = -1

            if opcion == 1:

                total = Lista_Precio[0] * 1.07
                comida = str(Lista_comida[0])
                break
            elif opcion == 2:

                total = Lista_Precio[1] * 1.07
                comida = str(Lista_comida[1])
                break
            elif opcion == 3:

                total = Lista_Precio[2] * 1.07
                comida = str(Lista_comida[2])
                break
            else:
                print("Opcion invalida")

        print("{}, debe pagar ${:.2f}".format(Nombre.Nombre, total))

        Dinero = float(input("Introduzca monto a pagar: "))

        if Dinero > total:

            Vuelto = Dinero - total

            print("Toma tu " + comida, ",", Nombre.Nombre)
            print("Aqui tiene su vuelto ${:.2f}".format(Vuelto))
            Ventas = open("/home/Sebastianrza/Desktop/Practica/La_Sabrosona /Ventas.txt", "a")
            Ventas.write("\nCliente:" + Nombre.Nombre)
            Ventas.write("\nCompro:" + comida)
            Ventas.write("\n" + time.strftime("%d/%m/%y"))
            Ventas.write("\n" + time.strftime("%I:%M:%S"))
            Ventas.write("\nMonto total: " + str(total))
            Ventas.write("\n")

            ventas_real_time += total
            Ventas.write("\n\n\nMonto de Ventas: " + str(ventas_real_time))
            Ventas.close()

        else:
            print("PEDIDO CANCELADO!!!!!!!")
            No_Completado = open("/home/Sebastianrza/Desktop/Practica/La_Sabrosona /Incompletas.txt", "a")
            No_Completado.write("\nCliente:" + Nombre.Nombre)
            No_Completado.write("\nIba a comprar:" + comida)
            No_Completado.write("\n" + time.strftime("%d/%m/%y"))
            No_Completado.write("\n" + time.strftime("%I:%M:%S"))
            No_Completado.write("\n")
            No_Completado.close()
